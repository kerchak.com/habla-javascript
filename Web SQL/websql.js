document.addEventListener("DOMContentLoaded", function() {
    var db = openDatabase('tareas', '1.0', 'mi base de datos de tareas', 2 * 1024 * 1024);
    db.transaction(function (tr) {
        // tr.executeSql('DELETE FROM mistareas');
        tr.executeSql('CREATE TABLE IF NOT EXISTS mistareas (id unique, text)');

        /* INSERTAR UNO O MÁS REGISTROS EN LA TABLA 'MISTAREAS' */
        tr.executeSql('SELECT * FROM mistareas ORDER BY id DESC LIMIT 1', [], function (tr, results) {
            var len = results.rows.length, i;
            if (len == '') {    //La tabla MISTAREAS está vacía
                console.log('LEN está vacío');
                id = 1;
            } else {            //Existen registros en la tabla
                id = results.rows.item(i).id;
                id++;
            }
            tr.executeSql('INSERT INTO mistareas (id, text) VALUES (' + id +', "Así se visualiza una tarea")');
        });

        /* ELIMINAR UN REGISTRO ESPECÍFICO DE LA TABLA EN CUESTIÓN */
        // tr.executeSql('DELETE FROM mistareas WHERE id = 8');

        /* LEER LOS REGISTROS Y CARGARLOS EN EL DOCUMENTO HTML */
        tr.executeSql('SELECT * FROM mistareas ORDER BY id', [], function (tr, results) {
            var len = results.rows.length;
            var html = "";
            for (i = 0; i <= len; i++) {
                //var registro = results.rows.item(i).id + " - " + results.rows.item(i).text;
                html = html + "<p>" + results.rows.item(i).id + " - " + results.rows.item(i).text + "</p>";
                var posicion = document.getElementById("tareas");
                posicion.innerHTML = html;
                
            }
    });
      });
});